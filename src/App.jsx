import React from "react";
import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./Components/Navbar/Navbar";
import Home from "./Components/Home/Home";
import Search from "./Components/Search/Search";
import Trending from "./Components/Trending/Trending";
import Sellers from "./Components/Sellers/Sellers";
import Auction from "./Components/Auction/Auction";
import Review from "./Components/Review/Review";
// import Footer from "./Components/Footer/Footer";
import NavbarPage from "./Components/NavbarPage/NavbarPage";
import Banner from "./Components/Banner/Banner";
import Jolly from "./Components/Jolly/Jolly";
import Jump from "./Components/Jump/Jump";

const App = () => {
  return (
    <Router>
      <div className="title">
        <Navbar />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/navbar-page" element={<NavbarPage />} />
          <Route path="/Jolly" element={<Jolly />} />
          <Route path="/Jump" element={<Jump />} />
        </Routes>
        {/* <Footer /> */}
      </div>
    </Router>
  );
};

const MainPage = () => {
  return (
    <div>
      <Banner />
      <Home />
      <Search />
      <Trending />
      <Sellers />
      <Auction />
      <Review />
    </div>
  );
};

export default App;
