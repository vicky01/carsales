import React, { useEffect } from "react";
import "./NavbarPage.css";
import image1 from "../../Assets/car back.webp";
import image2 from "../../Assets/car rain.webp";
import image3 from "../../Assets/car top.webp";
import image4 from "../../Assets/car rain.webp";
import image5 from "../../Assets/car back.webp";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faCalendarDay,
  faShieldHalved,
  faGasPump,
  faStar,
  faStarHalfAlt,
  faCarOn,
  faGauge,
  faBuilding,
  faUserCheck,
  faWrench,
  faGear,
  faLightbulb,
  faClockRotateLeft,
  faPhone,
} from "@fortawesome/free-solid-svg-icons";
import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

const NavbarPage = () => {
  const phoneNumber = "9486484866"; // Replace with the actual phone number

  const handleCallClick = () => {
    window.location.href = `tel:${phoneNumber}`;
  };
  const [modalShow, setModalShow] = React.useState(false);

  function MyVerticallyCenteredModal(props) {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Contact Us
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="car-info">
            <h1>Car Overview</h1>
            <div className="car-info-table">
              <div className="car-info-column">
                <div className="car-info-row">
                  <div className="car-info-item">
                    <FontAwesomeIcon icon={faCalendarDay} /> Registration Year
                  </div>
                  <div className="car-info-item-value">Oct 2021</div>
                </div>

                <div className="car-info-row">
                  <div className="car-info-item">
                    <FontAwesomeIcon icon={faBuilding} /> RTO
                  </div>
                  <div className="car-info-item-value">KA05</div>
                </div>
                <div className="car-info-row">
                  <div className="car-info-item">
                    <FontAwesomeIcon icon={faGear} /> Transmission
                  </div>
                  <div className="car-info-item-value">Manual</div>
                </div>
                <div className="car-info-row">
                  <div className="car-info-item">
                    <FontAwesomeIcon icon={faClockRotateLeft} /> Year of
                    Manufacture
                  </div>
                  <div className="car-info-item-value">2021</div>
                </div>
                <div className="car-info-row">
                  <div className="car-info-item">
                    <FontAwesomeIcon icon={faShieldHalved} /> Insurance Validity
                  </div>
                  <div className="car-info-item-value">Comprehensive</div>
                </div>
              </div>
              <div className="car-info-column">
                <div className="car-info-row">
                  <div className="car-info-item">
                    <FontAwesomeIcon icon={faGasPump} /> Fuel Type
                  </div>
                  <div className="car-info-item-value">Diesel</div>
                </div>
                <div className="car-info-row">
                  <div className="car-info-item">
                    <FontAwesomeIcon icon={faCarOn} /> Seats
                  </div>
                  <div className="car-info-item-value">5 Seats</div>
                </div>

                <div className="car-info-row">
                  <div className="car-info-item">
                    <FontAwesomeIcon icon={faUserCheck} /> Ownership
                  </div>
                  <div className="car-info-item-value">First Owner</div>
                </div>
                <div className="car-info-row">
                  <div className="car-info-item">
                    <FontAwesomeIcon icon={faGauge} /> Kms Driven
                  </div>
                  <div className="car-info-item-value">31,344 Kms</div>
                </div>
                <div className="car-info-row">
                  <div className="car-info-item">
                    <FontAwesomeIcon icon={faWrench} /> Engine Displacement
                  </div>
                  <div className="car-info-item-value">1956 cc</div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={props.onHide}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  useEffect(() => {
    const imgs = document.querySelectorAll(".img-select a");
    const imgBtns = [...imgs];
    let imgId = 1;

    imgBtns.forEach((imgItem) => {
      imgItem.addEventListener("click", (event) => {
        event.preventDefault();
        imgId = imgItem.dataset.id;
        slideImage();
      });
    });

    function slideImage() {
      const displayWidth = document.querySelector(
        ".img-showcase img:first-child"
      ).clientWidth;

      document.querySelector(".img-showcase").style.transform = `translateX(${
        -(imgId - 1) * displayWidth
      }px)`;
      document.querySelector(".img-showcase").style.transform = `translateX(${
        -(imgId - 1) * displayWidth
      }px)`;
    }

    window.addEventListener("resize", slideImage);

    // Clean up the event listeners when the component unmounts
    return () => {
      imgBtns.forEach((imgItem) => {
        imgItem.removeEventListener("click", slideImage);
      });
      window.removeEventListener("resize", slideImage);
    };
  }, []);

  return (
    <div>
      <div className="card-wrapper">
        <div className="card">
          <div className="product-imgs">
            <div className="img-display">
              <div className="img-showcase">
                <img src={image1} alt="shoe image" />
                <img src={image2} alt="shoe image" />
                <img src={image3} alt="shoe image" />
                <img src={image4} alt="shoe image" />
                <img src={image5} alt="shoe image" />
              </div>
            </div>
            <div className="img-select">
              <div className="img-item">
                <a href="#" data-id="1">
                  <img src={image1} alt="shoe image" />
                </a>
              </div>
              <div className="img-item">
                <a href="#" data-id="2">
                  <img src={image2} alt="shoe image" />
                </a>
              </div>
              <div className="img-item">
                <a href="#" data-id="3">
                  <img src={image3} alt="shoe image" />
                </a>
              </div>
              <div className="img-item">
                <a href="#" data-id="4">
                  <img src={image4} alt="shoe image" />
                </a>
              </div>
              <div className="img-item">
                <a href="#" data-id="5">
                  <img src={image5} alt="shoe image" />
                </a>
              </div>
            </div>
          </div>
          <div className="product-content">
            <h2 className="product-title">Tata Nexon</h2>
            <a href="#" className="product-link">
              visit Nanba cars
            </a>
            <div className="product-rating">
              <FontAwesomeIcon icon={faStar} />
              <FontAwesomeIcon icon={faStar} />
              <FontAwesomeIcon icon={faStar} />
              <FontAwesomeIcon icon={faStar} />
              <FontAwesomeIcon icon={faStarHalfAlt} />
              <span>4.7(21)</span>
            </div>
            <div className="product-price">
              <p className="last-price">
                Old Price: <span>$257.00</span>
              </p>
              <p className="new-price">
                New Price: <span>$249.00 (5%)</span>
              </p>
            </div>
            <div className="product-detail">
              <h2>about this car</h2>
              <h3>pros:</h3>
              <p>
                Pros The exterior styling is extensively tweaked to make it look
                attractive and sharp. Six airbags (standard), all seats have
                three-point seat belts, expect a high safety rating. The now
                all-black cabin gets revised styling elements and a long list of
                features. Strong mid-range, solid high-speed cruisability,
                frequent gear shifts are not needed. The well-judged suspension
                setup lends an overall comfortable ride.
              </p>
              <h3>cons:</h3>
              <p>
                Compared to its segment rivals, the fit and finish in some areas
                is still lacking. The Nexon's diesel engine is not as refined as
                the segment competitors.
              </p>
              <ul>
                <li>
                  Color: <span>Black</span>
                </li>
                <li>
                  Available: <span>in stock</span>
                </li>
                <li>
                  Category: <span>car</span>
                </li>
                <li>
                  Shipping Area: <span>All over the world</span>
                </li>
                <li>
                  Shipping Fee: <span>Free</span>
                </li>
              </ul>
            </div>
            <div className="purchase-info">
              {/* <input type="number" min="0" value="1" /> */}
              <button type="button" className="btn" onClick={handleCallClick}>
                call here
                <FontAwesomeIcon icon={faPhone} style={{ color: "#ff0000" }} />
              </button>
              <button
                type="button"
                className="btn"
                onClick={() => {
                  window.location.href = "https://wa.me/9486484866";
                }}
              >
                Whatsapp
                <FontAwesomeIcon icon={faWhatsapp} />
              </button>
              <div className="btn blue" onClick={() => setModalShow(true)}>
                Know More
                <FontAwesomeIcon icon={faLightbulb} />
              </div>
              <MyVerticallyCenteredModal
                show={modalShow}
                onHide={() => setModalShow(false)}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NavbarPage;
{
  /* <div className="social-links">
              <p>Share At: </p>
              <a href="#">
                <FontAwesomeIcon icon={["fab", "facebook-f"]} />
              </a>
              <a href="#">
                <FontAwesomeIcon icon={["fab", "twitter"]} />
              </a>
              <a href="#">
                <FontAwesomeIcon icon={["fab", "instagram"]} />
              </a>
              <a href="#">
                <FontAwesomeIcon icon={["fab", "whatsapp"]} />
              </a>
              <a href="#">
                <FontAwesomeIcon icon={["fab", "pinterest"]} />
              </a>
            </div> */
}
