import React, { useRef, useState } from "react";
import emailjs from "@emailjs/browser";
import "./Jump.css"; // Import your CSS file

const Contact = () => {
  const form = useRef();

  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_kd1pz3p",
        "template_xzq2q3g",
        form.current,
        "6dKuaT8XOO-LP_W6v"
      )
      .then(
        (result) => {
          console.log(result.text);
        },
        (error) => {
          console.log(error.text);
        }
      );
    e.target.reset();
  };

  return (
    <section className="flex-container">
      {" "}
      {/* Add the flex-container class */}
      <div className="container">
        <h2 className="text-center">Contact Us</h2>
        <form
          ref={form}
          onSubmit={sendEmail}
          className="form-control-card dir-column"
        >
          <div className="form-group">
            <label htmlFor="user_name">Full Name</label>
            <input type="text" id="user_name" name="user_name" required />
          </div>
          <div className="form-group">
            <label htmlFor="user_email">Email</label>
            <input type="email" id="user_email" name="user_email" required />
          </div>
          <div className="form-group">
            <label>Location</label>
            <select id="Ownership" name="Ownership">
              <option value="Mayiladuthuurai">Mayiladuthurai</option>
              <option value="Mannargudi">Mannargudi</option>
              <option value="Kumbakonam">Kumbakonam</option>
              <option value="Chidambaram">Chidambaram</option>
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="subject">Phonenumber</label>
            <input type="tel" id="subject" name="Phonenumber" required />
          </div>
          <div className="form-group">
            <label htmlFor="Adhar Number">Adhar Number</label>
            <input type="text" id="Adhar Number" name="Adhar Number" required />
          </div>
          <div className="form-group">
            <label htmlFor="Pan Card">Pan Card</label>
            <input type="text" id="Pan Card" name="Pan Card" required />
          </div>
          <div className="form-group">
            <label htmlFor="Your Bank">Your Bank</label>
            <input
              type="text"
              id="Your Bank"
              placeholder="State Bank"
              name="Your Bank"
              required
            />
          </div>
          <div className="form-group">
            <label htmlFor="Annual Income">Annual Income</label>
            <input
              type="text"
              id="Annual Income"
              name="Annual Income"
              required
            />
          </div>

          <div className="form-group">
            <label htmlFor="message">Message</label>
            <textarea id="message" name="message" cols="10" rows="2"></textarea>
          </div>
          <button type="submit" className="btn btn-primary">
            Send Message
          </button>
        </form>
      </div>
    </section>
  );
};

export default Contact;
