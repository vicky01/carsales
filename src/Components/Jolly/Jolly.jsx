// Jolly.jsx
import React, { useRef, useState } from "react";
import emailjs from "@emailjs/browser";
import "./Jolly.css"; // Import your CSS file

const Contact = () => {
  const form = useRef();
  const [selectedOption, setSelectedOption] = useState("1");
  const [customOption, setCustomOption] = useState("");
  const [isOtherChecked, setIsOtherChecked] = useState(false);

  const sendEmail = (e) => {
    e.preventDefault();
    const optionToSubmit = isOtherChecked ? customOption : selectedOption;

    emailjs
      .sendForm(
        "service_kd1pz3p",
        "template_o11fvfo",
        form.current,
        "6dKuaT8XOO-LP_W6v",
        {
          selectedOption: optionToSubmit,
          brand_name: optionToSubmit,
        }
      )
      .then(
        (result) => {
          console.log(result.text);
        },
        (error) => {
          console.log(error.text);
        }
      );
    e.target.reset();

  };

  return (
    <section className="flex-container">
      <div className="container">
        <h2 className="text-center">Contact Us</h2>
        <form
          ref={form}
          onSubmit={sendEmail}
          className="form-control-card dir-column"
        >
          {/* First Row */}
          <div className="form-row">
            <div className="form-group">
              <label htmlFor="user_name">Full Name</label>
              <input type="text" id="user_name" name="user_name" required />
            </div>
            <div className="form-group">
              <label htmlFor="user_email">Email</label>
              <input type="email" id="user_email" name="user_email" required />
            </div>
            <div className="form-group">
              <label>Location</label>
              <select id="Ownership" name="Ownership">
                <option value="Mayiladuthuurai">Mayiladuthurai</option>
                <option value="Mannargudi">Mannargudi</option>
                <option value="Kumbakonam">Kumbakonam</option>
                <option value="Chidambaram">Chidambaram</option>
              </select>
            </div>
            <div className="form-group">
              <label htmlFor="year_of_registration">Year Of Registration</label>
              <input
                type="date"
                id="year_of_registration"
                name="year_of_registration"
              />
            </div>
            <div className="form-group">
              <label htmlFor="subject">Phonenumber</label>
              <input type="tel" id="subject" name="Phonenumber" required />
            </div>
            <div className="form-group">
              <label htmlFor="carnumber">Car Number</label>
              <input
                type="text"
                id="carnumber"
                placeholder="TN8248"
                name="carnumber"
                required
              />
            </div>
          </div>

          {/* Second Row */}
          <div className="form-row">
            <div className="form-group">
              <label>Brand Name</label>
              <select
                name="brand_name"
                value={selectedOption}
                onChange={(e) => {
                  setSelectedOption(e.target.value);
                  setIsOtherChecked(e.target.value === "other");
                }}
              >
                {/* Options for Brand Name */}
              </select>
            </div>
            {isOtherChecked ? (
              <div className="form-group">
                <input
                  type="text"
                  placeholder="Type Other Option"
                  value={customOption}
                  onChange={(e) => setCustomOption(e.target.value)}
                />
              </div>
            ) : null}
            {/* Add other form fields for the second row */}
          </div>

          {/* Common Fields */}
          <div className="form-group">
            <label htmlFor="Car Model">Car Model</label>
            <input
              type="text"
              id="Car Model"
              placeholder="Nano"
              name="Car Model"
              required
            />
          </div>
          <div className="form-group">
            <label>Car Type</label>
            <select id="CarType" name="CarType">
              {/* Options for Car Type */}
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="Km Driven">Km Driven</label>
            <input type="number" id="Km Driven" name="Km Driven" required />
          </div>
          <div className="form-group">
            <label>Ownership</label>
            <select id="Ownership" name="Ownership">
              {/* Options for Ownership */}
            </select>
          </div>
          <div className="form-group">
            <label htmlFor="Insurance Validity ">Insurance Validity</label>
            <input
              type="date"
              id="Insurance Validity"
              name="Insurance Validity"
            />
          </div>
          <div className="form-group">
            <label htmlFor="Last Service Date">Last Service Date</label>
            <input
              type="date"
              id="Last Service Date"
              name="Last Service Date"
            />
          </div>

          <div className="form-group">
            <label htmlFor="message">Message</label>
            <textarea id="message" name="message" cols="10" rows="2"></textarea>
          </div>
          <button type="submit" className="btn btn-primary">
            Send Message
          </button>
        </form>
      </div>
    </section>
  );
};

export default Contact;
