import React, { useState, useEffect } from "react";
import "./Navbar.css";
import logo from "../../Assets/Nanbalogo1.png";
import { IoIosCloseCircle } from "react-icons/io";
import { TbGridDots } from "react-icons/tb";
import { Link } from "react-router-dom"; // Import Link from react-router-dom
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";

const Navbar = () => {
  const [navbar, setNavbar] = useState("navbar");

  const showNavbar = () => {
    setNavbar("navbar showNavbar");
  };

  const removeNavbar = () => {
    setNavbar("navbar");
  };

  const phoneNumber = "9486484866";

  const handleCallClick = () => {
    window.location.href = `tel:${phoneNumber}`;
  };

  const [header, setHeader] = useState("header");
  useEffect(() => {
    const addBg = () => {
      if (window.scrollY >= 20) {
        setHeader("header addBg");
      }
    };
    window.addEventListener("scroll", addBg);

    return () => {
      window.removeEventListener("scroll", addBg);
    };
  }, []);

  const [modalShow, setModalShow] = React.useState(false);

  function MyVerticallyCenteredModal(props) {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Contact Us
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h4>Centered Modal</h4>
          <p>1/29, main road</p>
          <button type="button" className="text" onClick={handleCallClick}>
            call here
          </button>
          <button
            type="button"
            className="text"
            onClick={() => {
              window.location.href = "https://wa.me/9486484866";
            }}
          >
            Whatsapp
          </button>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={props.onHide}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    );
  }

  return (
    <div className={header}>
      <div className="logoDiv">
        <img src={logo} alt="logo image" className="logo" />
      </div>
      <div className={navbar}>
        <ul className="menu">
          <li onClick={removeNavbar} className="listItem">
            <Link to="/" className="link">
              Home
            </Link>
          </li>
          <li onClick={removeNavbar} className="listItem">
            <Link to="/Jolly" className="link">
              Sell
            </Link>
          </li>
          <li onClick={removeNavbar} className="listItem">
            <Link to="/Jump" className="link">
              Finance
            </Link>
          </li>
        </ul>

        <IoIosCloseCircle className="icon closeIcon" onClick={removeNavbar} />
      </div>
      <div className="signUp flex">
        <div className="text" onClick={() => setModalShow(true)}>
          Nanba Car
        </div>
        <TbGridDots className="icon toggleNavbarIcon" onClick={showNavbar} />
        <MyVerticallyCenteredModal
          show={modalShow}
          onHide={() => setModalShow(false)}
        />
      </div>
    </div>
  );
};

export default Navbar;
